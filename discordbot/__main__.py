import os
from bot import DiscordBot

bot = DiscordBot()

if __name__ == "__main__":
    if os.name != "nt":
        import uvloop
        uvloop.install()
    bot.run()
