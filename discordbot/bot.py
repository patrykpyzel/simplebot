__all__ = ["DiscordBot"]

import os
import itertools
import logging
import typing
import yaml

import hikari
import lightbulb
from lightbulb.ext import tasks
import miru

_LOGGER = logging.getLogger("discordbot.bot")


class DiscordBot(lightbulb.BotApp):
    """Subclass of lightbulb's Bot"""

    def __init__(self):
        self.categories: typing.Dict[str, typing.List[lightbulb.Plugin]] = {}
        self.command_names: typing.Iterable[str] = []
        self.config = yaml.load(open("shared/config.yml"), Loader=yaml.FullLoader)

        self.dev_logs: hikari.GuildTextChannel = self.config["dev_logs"]
        self.dev_join_leave_logs: hikari.GuildTextChannel = self.config["dev_join_leave_logs"]
        self.zagadka_guild = self.config["zagadka_guild"]

        super().__init__(
            prefix=self.config["prefix"],
            case_insensitive_prefix_commands=True,
            token=os.environ["TOKEN"],
            banner=None,
            intents=hikari.Intents.ALL_UNPRIVILEGED | hikari.Intents.GUILD_MEMBERS,
            default_enabled_guilds=self.config["guilds"]
        )

    def run(self, **kwargs) -> None:
        self.event_manager.subscribe(hikari.StartingEvent, self.on_starting)
        self.event_manager.subscribe(hikari.StartedEvent, self.on_started)
        self.event_manager.subscribe(hikari.StoppingEvent, self.on_stopping)

        super().run(
            activity=hikari.Activity(
                name=f"Bot running...\n",
                type=hikari.ActivityType.PLAYING,
            ),
            **kwargs
        )

    async def on_starting(self, event: hikari.StartingEvent) -> None:
        """Load extensions when bot is starting."""
        _LOGGER.info("Connecting...")
        _LOGGER.info("Loading extensions")
        self.load_extensions_from("./modules/", must_exist=True, recursive=True)
        _LOGGER.info("Done loading extensions.")

    async def on_started(self, event: hikari.StartedEvent) -> None:
        """Notify dev-logs."""
        self.dev_logs = await self.rest.fetch_channel(self.dev_logs)
        self.dev_join_leave_logs = await self.rest.fetch_channel(self.dev_join_leave_logs)
        await self.dev_logs.send(f"Bot online!")
        _LOGGER.info("Loading tasks...")
        miru.load(self)
        tasks.load(self)
        _LOGGER.info('Bot is ready.')

    @tasks.task(s=5)
    async def print_every_5_seconds(self):
        print("Task called")

    async def on_stopping(self, event: hikari.StoppingEvent) -> None:
        """Disconnect BOT."""
        await self.dev_logs.send(f'Bot offline!')
