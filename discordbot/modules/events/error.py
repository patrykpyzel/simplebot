import hikari
import lightbulb
import logging

error_events = lightbulb.Plugin('Error', 'Error events', include_datastore=False)


async def check_instances(e, ctx):
    if type(e) is lightbulb.CheckFailure:
        return await check_instances(e.causes[0], ctx)
    elif isinstance(e, lightbulb.CommandNotFound):
        return True
    elif isinstance(e, lightbulb.MissingRequiredPermission):
        return await ctx.respond('Nie posiadasz permisji wymaganych do użycia komendy.', reply=True)
    elif isinstance(e, lightbulb.BotMissingRequiredPermission):
        await ctx.respond('Nie posiadam wystarczających permisji.', reply=True)
        raise e
    elif isinstance(e, lightbulb.errors.CommandIsOnCooldown):
        return await ctx.respond('Używasz komend zbyt szybko.', reply=True)
    elif isinstance(e, lightbulb.errors.NotEnoughArguments):
        return await ctx.respond('Komenda wymaga dodania argumentu.', reply=True)
    elif isinstance(e, lightbulb.CommandInvocationError):
        await ctx.respond('Nie posiadam uprawnień by wykonać komendę.', reply=True)
        raise e


@error_events.listener(lightbulb.CommandErrorEvent)
async def on_error(event: lightbulb.CommandErrorEvent) -> None:
    if await check_instances(event.exception, event.context):
        return
    raise event.exception


def load(bot) -> None:
    bot.add_plugin(error_events)


def unload(bot) -> None:
    bot.remove_plugin(error_events)
