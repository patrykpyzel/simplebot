from datetime import datetime
from lightbulb.ext import filament
import hikari
import lightbulb

user_plugin = lightbulb.Plugin('user', 'Komendy użytkownika')


@user_plugin.command
@lightbulb.option('target', 'Czyje ID chcesz sprawdzić?', hikari.User, required=False)
@lightbulb.command('id', 'Sprawdź avatar użytkownika.', auto_defer=True, ephemeral=False)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def user_id(ctx: lightbulb.Context, target: hikari.User):
    """Show id of user"""
    target = target or ctx.member or ctx.author
    await ctx.respond(str(target.id), reply=True)


def load(bot) -> None:
    bot.add_plugin(user_plugin)


def unload(bot) -> None:
    bot.remove_plugin(user_plugin)