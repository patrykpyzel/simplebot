# simplebot 

[![License](https://img.shields.io/badge/Licensed%20under-GPLv3-red.svg?style=flat-square)](./LICENSE) 
[![Made with Python](https://img.shields.io/badge/Made%20with-Python-ffde57.svg?longCache=true&style=flat-square&colorB=ffdf68&logo=python&logoColor=88889e)](https://www.python.org/) 
[![Powered by Hikari](https://img.shields.io/badge/Powered%20by-Hikari-ff69b4.svg?longCache=true&style=flat-square)](https://github.com/hikari-py/hikari) 
[![Server](https://img.shields.io/discord/560189268304330772.svg?style=flat-square&label=zaGadka)](https://discord.gg/zagadka)

The bot template for the [**zaGadka**](https://discord.gg/zagadka) server bots.

The premise of this template is to quickly create new bots using multiple sub-bots.

Simplebot currently runs on Python 3.10.5 and the latest hikari and hikari-lightbublb